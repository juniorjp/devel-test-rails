Sidekiq.configure_server do |config|
  config.redis = {namespace: 'sample'}
  database_url = ENV['DATABASE_URL']
  if(database_url)
    config.redis = {namespace: 'sample'}
    ENV['DATABASE_URL'] = "#{database_url}?pool=25"
    ActiveRecord::Base.establish_connection
  end


end