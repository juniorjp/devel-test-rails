unless ENV['USER'] == 'shippable' 

	env_file = File.dirname(__FILE__) + '/../.env'


	File.open( env_file ).each do |line|
  		env_data = line.split('=')
  		next if env_data.size <= 1
  		ENV[env_data[0].strip] = env_data[1].strip
	end
end
