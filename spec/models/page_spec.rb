require 'rails_helper'

RSpec.describe Page, :type => :model do

  describe Page, '.active' do
    it 'returns only active pages' do
      # setup
      active_page = create(:page, title: 'My active page', active: true)
      non_active_page = create(:page, title: 'My non active page', active: false)

      result = Page.active

      # verify
      expect(result).to eq [active_page]
    end

    it 'should require a title' do
      page = build(:page, title: '')

      #verify
      expect(page).not_to be_valid

      # teardown is handled for you by RSpec
    end

    it 'should not allow slugs with blank spaces' do
      page = build(:page, title: 'My Title', slug: 'My Slug')

      #verify
      expect(page).not_to be_valid

      # teardown is handled for you by RSpec
    end
  end
end
