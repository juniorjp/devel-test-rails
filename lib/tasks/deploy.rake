namespace :deploy do

task :production, [:type] => :environment do |t, args|
  host = '173.193.69.42'
  user = 'root'
  password = ''
  project_path = '/home/rildigital_oficial/'
  #repository = "git@bitbucket.org:juniorjp/final_rildigital.git"

  default_commands = [
      "cd #{project_path} && git pull origin master"
  ]

  precompile_commands = [
    "cd #{project_path} && source '/usr/local/rvm/scripts/rvm' && rake general:precompile RAILS_ENV=production"
  ]

  reload_commands = [
      "cd #{project_path} && source '/usr/local/rvm/scripts/rvm' && rake unicorn:reload"
  ]

  notice = 'Deploy without precompile finished.'

  if args.type == 'precompile'
    default_commands.push precompile_commands
    puts 'Start deploy with precompile.'
    notice = 'Deploy with precompile finished.'

  end

  default_commands.push reload_commands

  Net::SSH.start(host, user, :password => password) do |ssh|
    default_commands.each { |c| puts ssh.exec!(c) }
    ssh.loop
  end

  puts notice

end

end
