class Page < ActiveRecord::Base

  before_save :set_slug_if_blank
  before_save :remove_cache
  before_destroy :remove_cache
  def self.avalaible_positions
    total = self.count
    if total > 0
      (1..(total + 1)).to_a.map{|s| ["# #{s}", s]}
    else
      [["# 1", 1]]
    end
  end

  def self.active
    self.where(active: true)
  end

  validates :title, presence: true

  validates :slug, uniqueness: { :case_sensitive => false }, format: {with: /\A[A-Za-z\d_]+\z/, :message => I18n.t('activerecord.page.slug_not_in_format')},
            allow_blank: true

  def set_slug_if_blank
    if self.slug.blank?
      self.slug = self.title.downcase.tr(' ', '_')
    end
  end

  def remove_cache
    Rails.cache.clear
  end
end
