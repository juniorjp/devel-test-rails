ActiveAdmin.register Page do

  form do |f|
    f.inputs 'Fields' do
    f.input :title
    f.input :slug
    f.input :description
    f.input :body
    f.input :author
    f.input :active
    f.actions
    end
  end

  permit_params do
    permitted = [:title, :slug, :description, :author, :active ]
    permitted << :body
  end
  
end
