angular.module("Ignition").controller "PagesController",['$scope', 'Page', '$filter',  ($scope, Page, $filter) ->

  $scope.newPage = {}
  Page.index().$promise.then (data) ->
    $scope.pages = data.pages
    window.lol = $scope.pages
    return

  $scope.editPage = (page) ->
    Page.update(page, page.id).$promise.then (result) ->
      page = result
      alert("Página atualizada com sucesso!")
    return

  $scope.removePage = (page) ->
    Page.destroy(page, page.id).$promise.then (result) ->
      page = result
      alert("Página removida com sucesso!")
      $scope.pages = $filter('filter') $scope.pages, (value, index) ->
                       value.id != page.id
    return

  $scope.createPage = ->
    Page.create($scope.newPage).$promise.then (result) ->
      alert("Página criada com sucesso!")
      $scope.pages.push result
      $scope.newPage = {}
    return
  return

]