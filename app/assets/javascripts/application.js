// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery.js
//= require jquery.turbolinks
//= require jquery-ujs/src/rails.js
//= require ../../../vendor/assets/components/angular/angular.js
//= require ../../../vendor/assets/components/angular-resource/angular-resource.min.js
//= require ../../../vendor/assets/components/bootstrap/js/modal

//= require_tree .
//= require_tree ../../../vendor/assets/javascripts/.
//= require angular_bootstrap
//= require controllers/pages_controller

//= require turbolinks

