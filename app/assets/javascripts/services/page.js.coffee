'use strict'

angular.module('Ignition').service 'Page', ($resource) ->

  @index = ->
    $resource('/pages.json').get()

  @edit = (id) ->
    $resource("/pages/#{id}/edit.json", id: id).get()

  @create = (entry) ->
    $resource('/pages.json').save entry, ->
      console.log 'Página salva com sucesso'
      return

  @update = (entry, id) ->
    $resource("/pages/#{id}.json ", null, 'update': method: 'PUT').update entry, ->
      console.log 'Página atualizada com sucesso'
      return

  @destroy = (entry, id) ->
    $resource("/pages/#{id}.json ", null, 'update': method: 'POST').remove ->
      console.log 'Página removida com sucesso'
      return

  return