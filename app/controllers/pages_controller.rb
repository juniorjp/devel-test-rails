class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]

  #check authentication token only for json requests
  before_action :json_authentication_token

  # GET /pages
  # GET /pages.json
  def index
    @pages = Rails.cache.fetch("page/#{params[:page] || 0}",expires_in: 2.hours) do
      pages = Page.order('id DESC').page( params[:page] ).per(10)
      Kaminari::PaginatableArray.new(pages.to_a, limit: pages.limit_value, offset: pages.offset_value, total_count: pages.total_count)
    end

    respond_with @pages
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
  end

  def open
    @page = Page.find_by(url: params[:url])
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to pages_path, notice: I18n.t('page_conntroller.created') }
        format.json { render json: @page, status: :created, location: pages_path }
      else
        format.html {
          flash[:alert] = @page.errors.full_messages.to_sentence
          render :action => 'new'
        }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to pages_path, notice: I18n.t('page_conntroller.updated') }
        format.json { render json: @page, status: :created, location: pages_path}
      else
        format.html {
          flash[:alert] = @page.errors.full_messages.to_sentence
          render :action => 'new'
        }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url }
      format.json { render json: @page, status: :ok }
    end
  end

  private

  def json_authentication_token
    if request.format.json?
      auth_token = request.headers['Authorization']
      if auth_token
        authenticate_with_auth_token auth_token
      else
        authentication_error
      end
    end
  end

  def authenticate_with_auth_token auth_token
    unless auth_token == ENV['JSON_AUTHENTICATION_TOKEN']
      authentication_error
    end
  end

  def authentication_error
    render json: {error: I18n.t('unauthorized')}, status: 401
  end

  def set_page
    @page = Page.find(params[:id])
  end

  def page_params
    params.require(:page).permit(:title, :description, :slug, :body, :active, :author, :position, :insert_date, :update_date)
  end

end
