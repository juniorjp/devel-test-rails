# Teste 5

Responda as seguintes perguntas:

+ Para você, o que é o código ideal?
  > O código ideal deve ser um código legível, que segue os padrões adotados pela comunidade, eficiente e bem documentado, dessa forma levando em consideração que outros programadores irão manter o software no futuro. Como vivemos em um mundo rápido e bastante exigente, sabemos que não é possível fazer um código perfeito, mas devemos sempre mirar um código de qualidade mesmo com pouco tempo para implementá-lo.
+ Descreva o passo a passo de um workflow git
     >* Equipe crie a branch da sprint a partir da branch master. 
 > * Cada desenvolvedor cria uma branch para sua respectiva feature a partir da branch da sprint
 > * O desenvolvedor que terminar sua feature, deve fazer o push e enviar o Pull Request
 > * Outro membro da equipe deve revisar o Pull Request enviado
 > * Caso algum problema seja encontrado, o revisor deve notificar o desenvolvedor que criou a feature. Ao consertar o problema o desenvolvedor deve notificar novamente o revisor para continuar o processo de revisão
 >* Revisor aceita o Pull Request e faz o merge na branch da sprint
+ Qual o seu sistema operacional favorito? Justifique.
     > Ubuntu. É baseado em Linux, onde é possível realizarmos comandos poderosos pela linha de comando, seguro e estável( a versão 14 é bem estável ), funcionando bem tanto em Desktops quanto em Notebooks. Ao contrário do Windows, as gems do Ruby compilam perfeitamente, além de possuir o Redis onde podemos trabalhar tranquilamente com Background Queues, como por exemplo Sidekiq.
+ Qual o seu framework favorito? Justifique.
    >Ruby on Rails, antes de conhecer esse framework eu programava em Java, usando o vRaptor( desenvolvidos por brasileiros, muito bom por sinal) mas que não se compara com a syntax limpa do Ruby, além da agilidade e facilidade  que o Ruby/Rails lhe proporciona. O gerenciamento de depêndencias através das gems é infinitamente superior ao gerenciamento que eu obtia utilizando o Maven do Java. O ActiveRecord também infinitamente superior ao Hibernate, onde eu sempre me estressava com pool de sessões do banco de dados, no Rails tudo é tão simplificado, desenvolver se torna bem mais prazeroso! Minha qualidade de vida como programador melhorou bastante depois que aprendi a usar esse magnífico framework( e seus frameworks relacionados)!
+ Qual a sua linguagem de programação favorita? Justifique.
    >Ruby. ötima comunidade( quase sempre a gente acha uma gem que resolve o nosso problema), syntax limpa, fácil de aprender, ágil para programar. Com as novas versões 2.x garante uma melhora significativa de performance, então não é mais desculpa descartar Ruby por ser lenta, além de ter ótimos web servers como o Passenger, Unicorn e Puma. A Orientação a Objeto é bastante robusta, fácil de adicionar módulos e extender classes já existentes
+ Coloque aqui o link para seu github para que possamos avaliar qualquer projeto anterior seu ou contribuição com projetos de código aberto.
    >https://github.com/juniorjp      

Boa sorte!