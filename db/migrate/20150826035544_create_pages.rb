class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.text :description
      t.text :body
      t.string :author

      #I think the following fields are not necessary since we have rails default timestamps, but since they are mandatory fields:
      t.timestamp :insert_date
      t.timestamp :update_date

      #additional field:
      t.boolean :active
      t.integer :position
      t.timestamps null: false
    end
    add_index :pages, :slug
  end
end
